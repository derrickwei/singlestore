<?php

/* plaza/module/ptcontrolpanel.twig */
class __TwigTemplate_6007854d341f83e1155402205ced6d6d4a169eaa023110218caa63d74e65092a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-ptcontrolpanel\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            <ul class=\"breadcrumb theme-option-breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "            <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 22
        echo "        ";
        if ((isset($context["error_load_file"]) ? $context["error_load_file"] : null)) {
            // line 23
            echo "            <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_load_file"]) ? $context["error_load_file"] : null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 27
        echo "        ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 28
            echo "            <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 32
        echo "        <div class=\"row\">
            <div class=\"col-md-3 col-sm-12\">
                <div class=\"theme-option-container\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">";
        // line 37
        echo (isset($context["text_dashboard_menu"]) ? $context["text_dashboard_menu"] : null);
        echo "</h3>
                        </div>
                        <ul class=\"nav nav-tabs menu-sections pt-dashboard-menu\">
                            ";
        // line 40
        $context["i"] = 0;
        // line 41
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["plaza_menus"]) ? $context["plaza_menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 42
            echo "                                ";
            if ($this->getAttribute($context["menu"], "child", array())) {
                // line 43
                echo "                                    <li ";
                if ($this->getAttribute($context["menu"], "active", array())) {
                    echo " class=\"active\" ";
                }
                echo ">
                                        <a href=\"#ptcollapse_";
                // line 44
                echo (isset($context["i"]) ? $context["i"] : null);
                echo "\" data-toggle=\"collapse\" class=\"parent ";
                if ( !$this->getAttribute($context["menu"], "active", array())) {
                    echo " collapsed ";
                }
                echo "\">";
                echo $this->getAttribute($context["menu"], "title", array());
                echo "</a>
                                        <ul id=\"ptcollapse_";
                // line 45
                echo (isset($context["i"]) ? $context["i"] : null);
                echo "\" class=\"collapse ";
                if ($this->getAttribute($context["menu"], "active", array())) {
                    echo " in ";
                }
                echo "\">
                                            ";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menu"], "child", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 47
                    echo "                                                <li ";
                    if ($this->getAttribute($context["item"], "active", array())) {
                        echo " class=\"active\" ";
                    }
                    echo "><a href=\"";
                    echo $this->getAttribute($context["item"], "url", array());
                    echo "\">";
                    echo $this->getAttribute($context["item"], "title", array());
                    echo "</a></li>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "                                        </ul>
                                    </li>
                                ";
            } else {
                // line 52
                echo "                                    <li ";
                if ($this->getAttribute($context["menu"], "active", array())) {
                    echo " class=\"active\" ";
                }
                echo "><a href=\"";
                echo $this->getAttribute($context["menu"], "url", array());
                echo "\">";
                echo $this->getAttribute($context["menu"], "title", array());
                echo "</a></li>
                                ";
            }
            // line 54
            echo "                                ";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 55
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"col-md-9 col-sm-12\">
                <div class=\"theme-option-container\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\"><i class=\"fa fa-magic\"></i> ";
        // line 64
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
                            <div class=\"panel-select\">
                                <select id=\"input-store\" class=\"form-control\">
                                    ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 68
            echo "                                        <option value=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" ";
            if (($this->getAttribute($context["store"], "store_id", array()) == 0)) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["store"], "name", array());
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-3 col-left\">
                                <ul class=\"nav nav-tabs menu-sections\">
                                    <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\"><i class=\"fa fa-tachometer\" aria-hidden=\"true\"></i>";
        // line 76
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
                                    <li><a href=\"#tab-font\" data-toggle=\"tab\"><i class=\"fa fa-font\" aria-hidden=\"true\"></i>";
        // line 77
        echo (isset($context["tab_font"]) ? $context["tab_font"] : null);
        echo "</a></li>
                                    <li><a href=\"#tab-catalog\" data-toggle=\"tab\"><i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i>";
        // line 78
        echo (isset($context["tab_catalog"]) ? $context["tab_catalog"] : null);
        echo "</a></li>
                                    <li><a href=\"#tab-product\" data-toggle=\"tab\"><i class=\"fa fa-cube\" aria-hidden=\"true\"></i>";
        // line 79
        echo (isset($context["tab_product"]) ? $context["tab_product"] : null);
        echo "</a></li>
                                    <li><a href=\"#tab-category\" data-toggle=\"tab\"><i class=\"fa fa-tags\" aria-hidden=\"true\"></i>";
        // line 80
        echo (isset($context["tab_category"]) ? $context["tab_category"] : null);
        echo "</a></li>
                                    <li><a href=\"#tab-advance\" data-toggle=\"tab\"><i class=\"fa fa-fire\" aria-hidden=\"true\"></i>";
        // line 81
        echo (isset($context["tab_advance"]) ? $context["tab_advance"] : null);
        echo "</a></li>
                                </ul>
                            </div>
                            <div class=\"col-md-9 col-right\">
                                <div class=\"panel-body\">
                                    <form action=\"";
        // line 86
        echo (isset($context["action_import"]) ? $context["action_import"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-data\" class=\"form-horizontal\">
                                        <input type=\"hidden\" name=\"file\" />
                                    </form>
                                    <form action=\"";
        // line 89
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-ptcontrolpanel\" class=\"form-horizontal\">
                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_status\" value=\"1\" />
                                        <div class=\"tab-content\">
                                            <div class=\"tab-pane active\" id=\"tab-general\">
                                                ";
        // line 93
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 94
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 96
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <select name=\"module_ptcontrolpanel_header_layout[";
            // line 98
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" class=\"form-control\">
                                                                    <option value=\"1\" ";
            // line 99
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_header_layout"]) ? $context["module_ptcontrolpanel_header_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "1")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo " 1</option>
                                                                    <option value=\"2\" ";
            // line 100
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_header_layout"]) ? $context["module_ptcontrolpanel_header_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "2")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo " 2</option>
                                                                    <option value=\"3\" ";
            // line 101
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_header_layout"]) ? $context["module_ptcontrolpanel_header_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "3")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo " 3</option>
                                                                    <option value=\"4\" ";
            // line 102
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_header_layout"]) ? $context["module_ptcontrolpanel_header_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "4")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo " 4</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 108
            echo (isset($context["entry_responsive"]) ? $context["entry_responsive"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <select name=\"module_ptcontrolpanel_responsive_type[";
            // line 110
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" class=\"form-control\">
                                                                    <option value=\"responsive\" ";
            // line 111
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_responsive_type"]) ? $context["module_ptcontrolpanel_responsive_type"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "responsive")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_responsive_layout"]) ? $context["text_responsive_layout"] : null);
            echo "</option>
                                                                    <option value=\"specified\" ";
            // line 112
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_responsive_type"]) ? $context["module_ptcontrolpanel_responsive_type"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "specified")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_specified_layout"]) ? $context["text_specified_layout"] : null);
            echo "</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 118
            echo (isset($context["entry_sticky_header"]) ? $context["entry_sticky_header"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_sticky_header[";
            // line 120
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_sticky_header[";
            // line 121
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 122
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\"
                                                                        ";
            // line 123
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_sticky_header"]) ? $context["module_ptcontrolpanel_sticky_header"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 128
            echo (isset($context["entry_scroll_top"]) ? $context["entry_scroll_top"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_scroll_top[";
            // line 130
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_scroll_top[";
            // line 131
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 132
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\"
                                                                        ";
            // line 133
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_scroll_top"]) ? $context["module_ptcontrolpanel_scroll_top"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 138
            echo (isset($context["entry_lazy_load"]) ? $context["entry_lazy_load"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_lazy_load[";
            // line 140
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_lazy_load[";
            // line 141
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 142
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\"
                                                                        ";
            // line 143
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_lazy_load"]) ? $context["module_ptcontrolpanel_lazy_load"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 148
            echo (isset($context["entry_loader_image"]) ? $context["entry_loader_image"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <a href=\"\" id=\"thumb-image";
            // line 150
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
            echo $this->getAttribute((isset($context["thumb"]) ? $context["thumb"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" alt=\"\" title=\"\"  /></a>
                                                                <input type=\"hidden\" id=\"input-image";
            // line 151
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_loader_img[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_loader_img"]) ? $context["module_ptcontrolpanel_loader_img"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "                                            </div>

                                            <div class=\"tab-pane\" id=\"tab-font\">
                                                ";
        // line 159
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 160
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <ul class=\"nav nav-tabs hoz-ul-sections\">
                                                            <li class=\"active\"><a href=\"#tab-body-";
            // line 162
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_body"]) ? $context["tab_body"] : null);
            echo "</a></li>
                                                            <li><a href=\"#tab-header-";
            // line 163
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_heading"]) ? $context["tab_heading"] : null);
            echo "</a></li>
                                                            <li><a href=\"#tab-link-";
            // line 164
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_link"]) ? $context["tab_link"] : null);
            echo "</a></li>
                                                            <li><a href=\"#tab-button-";
            // line 165
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_button"]) ? $context["tab_button"] : null);
            echo "</a></li>
                                                        </ul>

                                                        <div class=\"tab-content child-content\">
                                                            <div class=\"tab-pane active\" id=\"tab-body-";
            // line 169
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-sm-3\"></div>
                                                                    <div class=\"col-sm-6\">
                                                                        <p class=\"font-body-demo-";
            // line 173
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " font-demo\">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br />
                                                                            a b c d e f g h i j k l m n o p q r s t u v w x y z <br />
                                                                            0 1 2 3 4 5 6 7 8 9</p>
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group font-control\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 180
            echo (isset($context["entry_global_font"]) ? $context["entry_global_font"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <select class=\"form-control\" name=\"module_ptcontrolpanel_body_font_family_id[";
            // line 182
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" onchange=\"chooseBodyFont(this.value, ";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ")\">
                                                                            <option>";
            // line 183
            echo (isset($context["text_choose_font"]) ? $context["text_choose_font"] : null);
            echo "</option>
                                                                            ";
            // line 184
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["fonts"]) ? $context["fonts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["font"]) {
                // line 185
                echo "                                                                                <option value=\"";
                echo $this->getAttribute($context["font"], "id", array());
                echo "\" ";
                if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_family_id"]) ? $context["module_ptcontrolpanel_body_font_family_id"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == $this->getAttribute($context["font"], "id", array()))) {
                    echo " selected=\"selected\" ";
                }
                // line 186
                echo "                                                                                        id=\"body-font-";
                echo $this->getAttribute($context["font"], "id", array());
                echo "\" data-family=\"";
                echo $this->getAttribute($context["font"], "family", array());
                echo "\" data-family-val=\"";
                echo $this->getAttribute($context["font"], "family_val", array());
                echo "\"
                                                                                        data-variants=\"";
                // line 187
                echo $this->getAttribute($context["font"], "variants", array());
                echo "\" data-subsets=\"";
                echo $this->getAttribute($context["font"], "subsets", array());
                echo "\" data-category=\"";
                echo $this->getAttribute($context["font"], "category", array());
                echo "\">
                                                                                    ";
                // line 188
                echo $this->getAttribute($context["font"], "family", array());
                echo "
                                                                                </option>
                                                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['font'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 191
            echo "                                                                        </select>
                                                                    </div>

                                                                    <input type=\"hidden\" id=\"body-font-family-name-";
            // line 194
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_body_font_family_name[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_body_font_family_name"]) ? $context["module_ptcontrolpanel_body_font_family_name"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    <input type=\"hidden\" id=\"body-font-family-cate-";
            // line 195
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_body_font_family_cate[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_body_font_family_cate"]) ? $context["module_ptcontrolpanel_body_font_family_cate"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    <input type=\"hidden\" id=\"body-font-family-link-";
            // line 196
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_body_font_family_link[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_body_font_family_link"]) ? $context["module_ptcontrolpanel_body_font_family_link"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 200
            echo (isset($context["entry_font_size"]) ? $context["entry_font_size"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input class=\"form-control\" type=\"text\" name=\"module_ptcontrolpanel_body_font_size[";
            // line 202
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_body_font_size"]) ? $context["module_ptcontrolpanel_body_font_size"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 207
            echo (isset($context["entry_font_weight"]) ? $context["entry_font_weight"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <select class=\"form-control\" id=\"body-font-weight-";
            // line 209
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_body_font_weight[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" onclick=\"chooseBodyWeight(this.value, ";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ")\">
                                                                            <option value=\"300\" ";
            // line 210
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_weight"]) ? $context["module_ptcontrolpanel_body_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "300")) {
                echo "selected=\"selected\"";
            }
            echo ">300</option>
                                                                            <option value=\"400\" ";
            // line 211
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_weight"]) ? $context["module_ptcontrolpanel_body_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "400")) {
                echo "selected=\"selected\"";
            }
            echo ">400</option>
                                                                            <option value=\"500\" ";
            // line 212
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_weight"]) ? $context["module_ptcontrolpanel_body_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "500")) {
                echo "selected=\"selected\"";
            }
            echo ">500</option>
                                                                            <option value=\"600\" ";
            // line 213
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_weight"]) ? $context["module_ptcontrolpanel_body_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "600")) {
                echo "selected=\"selected\"";
            }
            echo ">600</option>
                                                                            <option value=\"700\" ";
            // line 214
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_body_font_weight"]) ? $context["module_ptcontrolpanel_body_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "700")) {
                echo "selected=\"selected\"";
            }
            echo ">700</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 220
            echo (isset($context["entry_color"]) ? $context["entry_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_body_color[";
            // line 222
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_body_color"]) ? $context["module_ptcontrolpanel_body_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class=\"tab-pane\" id=\"tab-header-";
            // line 227
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-sm-3\"></div>
                                                                    <div class=\"col-sm-6\">
                                                                        <p class=\"font-heading-demo-";
            // line 231
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " font-demo\">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br />
                                                                            a b c d e f g h i j k l m n o p q r s t u v w x y z <br />
                                                                            0 1 2 3 4 5 6 7 8 9</p>
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group font-control\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 238
            echo (isset($context["entry_heading_font"]) ? $context["entry_heading_font"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <select class=\"form-control\" name=\"module_ptcontrolpanel_heading_font_family_id[";
            // line 240
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" onchange=\"chooseHeadingFont(this.value, ";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ")\">
                                                                            <option>";
            // line 241
            echo (isset($context["text_choose_font"]) ? $context["text_choose_font"] : null);
            echo "</option>
                                                                            ";
            // line 242
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["fonts"]) ? $context["fonts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["font"]) {
                // line 243
                echo "                                                                                <option value=\"";
                echo $this->getAttribute($context["font"], "id", array());
                echo "\" ";
                if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_family_id"]) ? $context["module_ptcontrolpanel_heading_font_family_id"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == $this->getAttribute($context["font"], "id", array()))) {
                    echo " selected=\"selected\" ";
                }
                // line 244
                echo "                                                                                        id=\"heading-font-";
                echo $this->getAttribute($context["font"], "id", array());
                echo "\" data-family=\"";
                echo $this->getAttribute($context["font"], "family", array());
                echo "\" data-family-val=\"";
                echo $this->getAttribute($context["font"], "family_val", array());
                echo "\"
                                                                                        data-variants=\"";
                // line 245
                echo $this->getAttribute($context["font"], "variants", array());
                echo "\" data-subsets=\"";
                echo $this->getAttribute($context["font"], "subsets", array());
                echo "\" data-category=\"";
                echo $this->getAttribute($context["font"], "category", array());
                echo "\">
                                                                                    ";
                // line 246
                echo $this->getAttribute($context["font"], "family", array());
                echo "
                                                                                </option>
                                                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['font'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 249
            echo "                                                                        </select>
                                                                    </div>

                                                                    <input type=\"hidden\" id=\"heading-font-family-name-";
            // line 252
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_heading_font_family_name[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_family_name"]) ? $context["module_ptcontrolpanel_heading_font_family_name"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    <input type=\"hidden\" id=\"heading-font-family-cate-";
            // line 253
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_heading_font_family_cate[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_family_cate"]) ? $context["module_ptcontrolpanel_heading_font_family_cate"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    <input type=\"hidden\" id=\"heading-font-family-link-";
            // line 254
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_heading_font_family_link[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_family_link"]) ? $context["module_ptcontrolpanel_heading_font_family_link"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 258
            echo (isset($context["entry_font_weight"]) ? $context["entry_font_weight"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <select class=\"form-control\" id=\"heading-font-weight-";
            // line 260
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" name=\"module_ptcontrolpanel_heading_font_weight[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" onchange=\"chooseHeadingWeight(this.value, ";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ")\">
                                                                            <option value=\"300\" ";
            // line 261
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_weight"]) ? $context["module_ptcontrolpanel_heading_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "300")) {
                echo "selected=\"selected\"";
            }
            echo ">300</option>
                                                                            <option value=\"400\" ";
            // line 262
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_weight"]) ? $context["module_ptcontrolpanel_heading_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "400")) {
                echo "selected=\"selected\"";
            }
            echo ">400</option>
                                                                            <option value=\"500\" ";
            // line 263
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_weight"]) ? $context["module_ptcontrolpanel_heading_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "500")) {
                echo "selected=\"selected\"";
            }
            echo ">500</option>
                                                                            <option value=\"600\" ";
            // line 264
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_weight"]) ? $context["module_ptcontrolpanel_heading_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "600")) {
                echo "selected=\"selected\"";
            }
            echo ">600</option>
                                                                            <option value=\"700\" ";
            // line 265
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_heading_font_weight"]) ? $context["module_ptcontrolpanel_heading_font_weight"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "700")) {
                echo "selected=\"selected\"";
            }
            echo ">700</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-3 control-label\">";
            // line 271
            echo (isset($context["entry_color"]) ? $context["entry_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_heading_color[";
            // line 273
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_heading_color"]) ? $context["module_ptcontrolpanel_heading_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class=\"tab-pane\" id=\"tab-link-";
            // line 278
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 280
            echo (isset($context["entry_color"]) ? $context["entry_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_link_color[";
            // line 282
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_link_color"]) ? $context["module_ptcontrolpanel_link_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 287
            echo (isset($context["entry_hover_color"]) ? $context["entry_hover_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_link_hover_color[";
            // line 289
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_link_hover_color"]) ? $context["module_ptcontrolpanel_link_hover_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class=\"tab-pane\" id=\"tab-button-";
            // line 294
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 296
            echo (isset($context["entry_color"]) ? $context["entry_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_button_color[";
            // line 298
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_button_color"]) ? $context["module_ptcontrolpanel_button_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 303
            echo (isset($context["entry_hover_color"]) ? $context["entry_hover_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_button_hover_color[";
            // line 305
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_button_hover_color"]) ? $context["module_ptcontrolpanel_button_hover_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 310
            echo (isset($context["entry_bg_color"]) ? $context["entry_bg_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_button_bg_color[";
            // line 312
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_button_bg_color"]) ? $context["module_ptcontrolpanel_button_bg_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\">";
            // line 317
            echo (isset($context["entry_bg_hover_color"]) ? $context["entry_bg_hover_color"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-3\">
                                                                        <input class=\"form-control jscolor\" type=\"text\" name=\"module_ptcontrolpanel_button_bg_hover_color[";
            // line 319
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_button_bg_hover_color"]) ? $context["module_ptcontrolpanel_button_bg_hover_color"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 326
        echo "                                                <input type=\"hidden\" value=\"";
        echo (isset($context["entry_font_weight"]) ? $context["entry_font_weight"] : null);
        echo "\" id=\"text-font-weight\" />
                                                <input type=\"hidden\" value=\"";
        // line 327
        echo (isset($context["entry_font_subset"]) ? $context["entry_font_subset"] : null);
        echo "\" id=\"text-font-subset\" />
                                            </div>

                                            <div class=\"tab-pane\" id=\"tab-product\">
                                                ";
        // line 331
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 332
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-related-";
            // line 334
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_related_pro"]) ? $context["entry_related_pro"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_related[";
            // line 336
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_related[";
            // line 337
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-related-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 338
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 339
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_related"]) ? $context["module_ptcontrolpanel_related"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-social-";
            // line 344
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_social_icons"]) ? $context["entry_social_icons"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_social[";
            // line 346
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_social[";
            // line 347
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-social-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 348
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 349
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_social"]) ? $context["module_ptcontrolpanel_social"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-tax-";
            // line 354
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_tax"]) ? $context["entry_tax"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_tax[";
            // line 356
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_tax[";
            // line 357
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-tax-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 358
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 359
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_tax"]) ? $context["module_ptcontrolpanel_tax"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-tags-";
            // line 364
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_tags"]) ? $context["entry_tags"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_tags[";
            // line 366
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_tags[";
            // line 367
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-tags-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 368
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 369
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_tags"]) ? $context["module_ptcontrolpanel_tags"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-zoom-";
            // line 374
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_use_zoom"]) ? $context["entry_use_zoom"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_use_zoom[";
            // line 376
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch zoom-status\" name=\"module_ptcontrolpanel_use_zoom[";
            // line 377
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-zoom-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 378
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 379
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_use_zoom"]) ? $context["module_ptcontrolpanel_use_zoom"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-zoom-type-";
            // line 384
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_zoom_type"]) ? $context["entry_zoom_type"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <select name=\"module_ptcontrolpanel_zoom_type[";
            // line 386
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-zoom-type-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\">
                                                                    <option value=\"outer\" ";
            // line 387
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_zoom_type"]) ? $context["module_ptcontrolpanel_zoom_type"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "outer")) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo (isset($context["text_outside"]) ? $context["text_outside"] : null);
            echo "</option>
                                                                    <option value=\"inner\" ";
            // line 388
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_zoom_type"]) ? $context["module_ptcontrolpanel_zoom_type"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "inner")) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo (isset($context["text_inside"]) ? $context["text_inside"] : null);
            echo "</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-zoom-space-";
            // line 394
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_zoom_space"]) ? $context["entry_zoom_space"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"text\" id=\"input-zoom-space-";
            // line 396
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\" name=\"module_ptcontrolpanel_zoom_space[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_zoom_space"]) ? $context["module_ptcontrolpanel_zoom_space"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" placeholder=\"";
            echo (isset($context["text_zoom_space"]) ? $context["text_zoom_space"] : null);
            echo "\" />
                                                            </div>
                                                        </div>
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-zoom-title-";
            // line 400
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_zoom_title"]) ? $context["entry_zoom_title"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_zoom_title[";
            // line 402
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_zoom_title[";
            // line 403
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-zoom-title-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 404
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 405
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_zoom_title"]) ? $context["module_ptcontrolpanel_zoom_title"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-swatches-";
            // line 410
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_use_swatches"]) ? $context["entry_use_swatches"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_use_swatches[";
            // line 412
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_use_swatches[";
            // line 413
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-swatches-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 414
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 415
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_use_swatches"]) ? $context["module_ptcontrolpanel_use_swatches"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-dimension-";
            // line 420
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_icon_swatches"]) ? $context["entry_icon_swatches"] : null);
            echo "</label>
                                                            <div class=\"col-sm-3\">
                                                                <input type=\"text\" value=\"";
            // line 422
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_swatches_width"]) ? $context["module_ptcontrolpanel_swatches_width"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" name=\"module_ptcontrolpanel_swatches_width[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" placeholder=\"";
            echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
            echo "\" class=\"form-control\" />
                                                            </div>
                                                            <div class=\"col-sm-3\">
                                                                <input type=\"text\" value=\"";
            // line 425
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_swatches_height"]) ? $context["module_ptcontrolpanel_swatches_height"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" name=\"module_ptcontrolpanel_swatches_height[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" placeholder=\"";
            echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
            echo "\" class=\"form-control\" />
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-color-option-";
            // line 430
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_color_option"]) ? $context["entry_color_option"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <select name=\"module_ptcontrolpanel_swatches_option[";
            // line 432
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-color-option-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\">
                                                                    ";
            // line 433
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 434
                echo "                                                                        ";
                if ((($this->getAttribute($context["option"], "type", array()) == "select") || ($this->getAttribute($context["option"], "type", array()) == "radio"))) {
                    // line 435
                    echo "                                                                            <option value=\"";
                    echo $this->getAttribute($context["option"], "option_id", array());
                    echo "\" ";
                    if (($this->getAttribute((isset($context["module_ptcontrolpanel_swatches_option"]) ? $context["module_ptcontrolpanel_swatches_option"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == $this->getAttribute($context["option"], "option_id", array()))) {
                        echo " selected=\"selected\" ";
                    }
                    echo ">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</option>
                                                                        ";
                }
                // line 437
                echo "                                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 438
            echo "                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 443
        echo "                                            </div>

                                            <div class=\"tab-pane\" id=\"tab-category\">
                                                ";
        // line 446
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 447
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-category-image-";
            // line 449
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_category_image"]) ? $context["entry_category_image"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_category_image[";
            // line 451
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_image[";
            // line 452
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-image-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 453
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 454
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_image"]) ? $context["module_ptcontrolpanel_category_image"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-category-des-";
            // line 459
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_category_des"]) ? $context["entry_category_des"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_category_description[";
            // line 461
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_description[";
            // line 462
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-des-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 463
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 464
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_description"]) ? $context["module_ptcontrolpanel_category_description"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-category-sub-";
            // line 469
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_sub_category"]) ? $context["entry_sub_category"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_sub_category[";
            // line 471
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_sub_category[";
            // line 472
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-sub-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 473
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 474
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_sub_category"]) ? $context["module_ptcontrolpanel_sub_category"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-filter-";
            // line 479
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_filter"]) ? $context["entry_filter"] : null);
            echo "</label>
                                                            <div class=\"col-sm-2\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_use_filter[";
            // line 481
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_use_filter[";
            // line 482
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-filter-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 483
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 484
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_use_filter"]) ? $context["module_ptcontrolpanel_use_filter"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                            <label class=\"col-sm-2 control-label\">";
            // line 486
            echo (isset($context["entry_position"]) ? $context["entry_position"] : null);
            echo "</label>
                                                            <div class=\"col-sm-2\">
                                                                <select name=\"module_ptcontrolpanel_filter_position[";
            // line 488
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" class=\"form-control\">
                                                                    <option value=\"left\" ";
            // line 489
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_filter_position"]) ? $context["module_ptcontrolpanel_filter_position"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "left")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_left"]) ? $context["text_left"] : null);
            echo "</option>
                                                                    <option value=\"right\" ";
            // line 490
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_filter_position"]) ? $context["module_ptcontrolpanel_filter_position"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "right")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_right"]) ? $context["text_right"] : null);
            echo "</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-cquickview-";
            // line 496
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_quickview"]) ? $context["entry_quickview"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_cate_quickview[";
            // line 498
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_cate_quickview[";
            // line 499
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-cquickview-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 500
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 501
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_cate_quickview"]) ? $context["module_ptcontrolpanel_cate_quickview"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-image-effect-";
            // line 506
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_img_effect"]) ? $context["entry_img_effect"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <select name=\"module_ptcontrolpanel_img_effect[";
            // line 508
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-image-effect-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\">
                                                                    <option value=\"none\" ";
            // line 509
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_img_effect"]) ? $context["module_ptcontrolpanel_img_effect"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "none")) {
                echo "selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_none"]) ? $context["text_none"] : null);
            echo "</option>
                                                                    <option value=\"hover\" ";
            // line 510
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_img_effect"]) ? $context["module_ptcontrolpanel_img_effect"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "hover")) {
                echo "selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_hover_img"]) ? $context["text_hover_img"] : null);
            echo "</option>
                                                                    <option value=\"swatches\" ";
            // line 511
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_img_effect"]) ? $context["module_ptcontrolpanel_img_effect"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "swatches")) {
                echo "selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_swatches_img"]) ? $context["text_swatches_img"] : null);
            echo "</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\">";
            // line 516
            echo (isset($context["entry_icon_swatches"]) ? $context["entry_icon_swatches"] : null);
            echo "</label>
                                                            <div class=\"col-sm-3\">
                                                                <input type=\"text\" value=\"";
            // line 518
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_cate_swatches_width"]) ? $context["module_ptcontrolpanel_cate_swatches_width"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" name=\"module_ptcontrolpanel_cate_swatches_width[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" placeholder=\"";
            echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
            echo "\" class=\"form-control\" />
                                                            </div>
                                                            <div class=\"col-sm-3\">
                                                                <input type=\"text\" value=\"";
            // line 521
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_cate_swatches_height"]) ? $context["module_ptcontrolpanel_cate_swatches_height"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "\" name=\"module_ptcontrolpanel_cate_swatches_height[";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" placeholder=\"";
            echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
            echo "\" class=\"form-control\" />
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-view-";
            // line 526
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_advance_view"]) ? $context["entry_advance_view"] : null);
            echo "</label>
                                                            <div class=\"col-sm-6\">
                                                                <input type=\"hidden\" name=\"module_ptcontrolpanel_advance_view[";
            // line 528
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_advance_view[";
            // line 529
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-view-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                       data-toggle=\"toggle\" data-on=\"";
            // line 530
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                        ";
            // line 531
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_advance_view"]) ? $context["module_ptcontrolpanel_advance_view"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                            </div>
                                                        </div>
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-default-view-";
            // line 535
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_default_view"]) ? $context["entry_default_view"] : null);
            echo "</label>
                                                            <div class=\"col-sm-3\">
                                                                <select name=\"module_ptcontrolpanel_default_view[";
            // line 537
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-default-view-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\">
                                                                    <option value=\"list\" ";
            // line 538
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_default_view"]) ? $context["module_ptcontrolpanel_default_view"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "list")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_list"]) ? $context["text_list"] : null);
            echo "</option>
                                                                    <option value=\"grid\" ";
            // line 539
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_default_view"]) ? $context["module_ptcontrolpanel_default_view"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "grid")) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo (isset($context["text_grid"]) ? $context["text_grid"] : null);
            echo "</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-4 control-label\" for=\"input-row-";
            // line 544
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_product_row"]) ? $context["entry_product_row"] : null);
            echo "</label>
                                                            <div class=\"col-sm-3\">
                                                                <select name=\"module_ptcontrolpanel_product_row[";
            // line 546
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-row-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" class=\"form-control\">
                                                                    <option value=\"2\" ";
            // line 547
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_product_row"]) ? $context["module_ptcontrolpanel_product_row"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "2")) {
                echo " selected=\"selected\" ";
            }
            echo ">2</option>
                                                                    <option value=\"3\" ";
            // line 548
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_product_row"]) ? $context["module_ptcontrolpanel_product_row"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "3")) {
                echo " selected=\"selected\" ";
            }
            echo ">3</option>
                                                                    <option value=\"4\" ";
            // line 549
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_product_row"]) ? $context["module_ptcontrolpanel_product_row"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "4")) {
                echo " selected=\"selected\" ";
            }
            echo ">4</option>
                                                                    <option value=\"5\" ";
            // line 550
            if (($this->getAttribute((isset($context["module_ptcontrolpanel_product_row"]) ? $context["module_ptcontrolpanel_product_row"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == "5")) {
                echo " selected=\"selected\" ";
            }
            echo ">5</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 556
        echo "                                            </div>

                                            <div class=\"tab-pane\" id=\"tab-catalog\">
                                                ";
        // line 559
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 560
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <ul class=\"nav nav-tabs hoz-ul-sections\">
                                                            <li class=\"active\"><a href=\"#tab-cheader-";
            // line 562
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_header"]) ? $context["tab_header"] : null);
            echo "</a></li>
                                                            <li><a href=\"#tab-product-";
            // line 563
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_product"]) ? $context["tab_product"] : null);
            echo "</a></li>
                                                            <li><a href=\"#tab-category-";
            // line 564
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_product_listing"]) ? $context["tab_product_listing"] : null);
            echo "</a></li>
                                                        </ul>

                                                        <div class=\"tab-content child-content\">
                                                            <div class=\"tab-pane active\" id=\"tab-cheader-";
            // line 568
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-header-cart-";
            // line 570
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_cart"]) ? $context["entry_cart"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_header_cart[";
            // line 572
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_header_cart[";
            // line 573
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-header-cart-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 574
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 575
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_header_cart"]) ? $context["module_ptcontrolpanel_header_cart"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-header-currency-";
            // line 580
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_currency"]) ? $context["entry_currency"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_header_currency[";
            // line 582
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_header_currency[";
            // line 583
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-header-currency-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 584
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 585
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_header_currency"]) ? $context["module_ptcontrolpanel_header_currency"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class=\"tab-pane\" id=\"tab-product-";
            // line 590
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-product-price-";
            // line 592
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_price"]) ? $context["entry_show_price"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_product_price[";
            // line 594
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_product_price[";
            // line 595
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-product-price-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 596
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 597
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_product_price"]) ? $context["module_ptcontrolpanel_product_price"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-product-cart-";
            // line 602
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_cart"]) ? $context["entry_show_cart"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_product_cart[";
            // line 604
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_product_cart[";
            // line 605
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-product-cart-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 606
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 607
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_product_cart"]) ? $context["module_ptcontrolpanel_product_cart"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-product-wishlist-";
            // line 612
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_wishlist"]) ? $context["entry_show_wishlist"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_product_wishlist[";
            // line 614
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_product_wishlist[";
            // line 615
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-product-wishlist-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 616
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 617
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_product_wishlist"]) ? $context["module_ptcontrolpanel_product_wishlist"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-product-compare-";
            // line 622
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_compare"]) ? $context["entry_show_compare"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_product_compare[";
            // line 624
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_product_compare[";
            // line 625
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-product-compare-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 626
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 627
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_product_compare"]) ? $context["module_ptcontrolpanel_product_compare"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-product-options-";
            // line 632
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_options"]) ? $context["entry_show_options"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_product_options[";
            // line 634
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_product_options[";
            // line 635
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-product-options-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 636
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 637
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_product_options"]) ? $context["module_ptcontrolpanel_product_options"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"tab-pane\" id=\"tab-category-";
            // line 641
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-price-";
            // line 643
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_price"]) ? $context["entry_show_price"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_price[";
            // line 645
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_price[";
            // line 646
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-price-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 647
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 648
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_price"]) ? $context["module_ptcontrolpanel_category_price"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-cart-";
            // line 653
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_cart"]) ? $context["entry_show_cart"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_cart[";
            // line 655
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_cart[";
            // line 656
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-cart-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 657
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 658
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_cart"]) ? $context["module_ptcontrolpanel_category_cart"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-wishlist-";
            // line 663
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_wishlist"]) ? $context["entry_show_wishlist"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_wishlist[";
            // line 665
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_wishlist[";
            // line 666
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-wishlist-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 667
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 668
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_wishlist"]) ? $context["module_ptcontrolpanel_category_wishlist"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-compare-";
            // line 673
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_show_compare"]) ? $context["entry_show_compare"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_compare[";
            // line 675
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_compare[";
            // line 676
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-compare-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 677
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 678
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_compare"]) ? $context["module_ptcontrolpanel_category_compare"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-prodes-";
            // line 683
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_product_des"]) ? $context["entry_product_des"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_prodes[";
            // line 685
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_prodes[";
            // line 686
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-prodes-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 687
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 688
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_prodes"]) ? $context["module_ptcontrolpanel_category_prodes"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>

                                                                <div class=\"form-group\">
                                                                    <label class=\"col-sm-4 control-label\" for=\"input-category-label-";
            // line 693
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo (isset($context["entry_label"]) ? $context["entry_label"] : null);
            echo "</label>
                                                                    <div class=\"col-sm-6\">
                                                                        <input type=\"hidden\" name=\"module_ptcontrolpanel_category_label[";
            // line 695
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" value=\"0\" />
                                                                        <input type=\"checkbox\" class=\"ckb-switch\" name=\"module_ptcontrolpanel_category_label[";
            // line 696
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" id=\"input-category-label-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                               data-toggle=\"toggle\" data-on=\"";
            // line 697
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "\" data-off=\"";
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "\" data-width=\"100\" data-height=\"36\" data-store-id=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\"
                                                                                ";
            // line 698
            if ($this->getAttribute((isset($context["module_ptcontrolpanel_category_label"]) ? $context["module_ptcontrolpanel_category_label"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array")) {
                echo " value=\"1\" checked ";
            } else {
                echo " value=\"0\" ";
            }
            echo ">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 705
        echo "                                            </div>

                                            <div class=\"tab-pane\" id=\"tab-advance\">
                                                ";
        // line 708
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 709
            echo "                                                    <div class=\"frm-field frm-field-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">
                                                        <div class=\"form-group\" >
                                                            <label class=\"col-sm-3 control-label\" for=\"input-theme\">";
            // line 711
            echo (isset($context["entry_theme_database"]) ? $context["entry_theme_database"] : null);
            echo "</label>
                                                            <div class=\"col-sm-9\">
                                                                <div class=\"row\">
                                                                    <div class=\"col-sm-6\">
                                                                        <select id=\"input-theme\" class=\"form-control\" name=\"file\">
                                                                            ";
            // line 716
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["database"]) ? $context["database"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 717
                echo "                                                                                <option value=\"";
                echo $context["key"];
                echo "\">";
                echo $context["value"];
                echo "</option>
                                                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 719
            echo "                                                                        </select>
                                                                    </div>
                                                                    <div class=\"col-sm-6\">
                                                                        <button type=\"button\" id=\"button-import\" class=\"btn btn-primary btn-theme-option\"><i class=\"fa fa-upload\"></i> ";
            // line 722
            echo (isset($context["button_import"]) ? $context["button_import"] : null);
            echo "</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-3 control-label\" for=\"input-theme\">";
            // line 729
            echo (isset($context["entry_custom_css"]) ? $context["entry_custom_css"] : null);
            echo "</label>
                                                            <div class=\"col-sm-9\">
                                                                <textarea class=\"form-control code-area\" rows=\"15\" name=\"module_ptcontrolpanel_custom_css[";
            // line 731
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\">";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_custom_css"]) ? $context["module_ptcontrolpanel_custom_css"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "</textarea>
                                                            </div>
                                                        </div>

                                                        <div class=\"form-group\">
                                                            <label class=\"col-sm-3 control-label\" for=\"input-theme\">";
            // line 736
            echo (isset($context["entry_custom_js"]) ? $context["entry_custom_js"] : null);
            echo "</label>
                                                            <div class=\"col-sm-9\">
                                                                <textarea class=\"form-control code-area\" rows=\"15\" name=\"module_ptcontrolpanel_custom_js[";
            // line 738
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\">";
            echo $this->getAttribute((isset($context["module_ptcontrolpanel_custom_js"]) ? $context["module_ptcontrolpanel_custom_js"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array");
            echo "</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 743
        echo "                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"body-stylesheet-link\"></div>
<div class=\"heading-stylesheet-link\"></div>
<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$('.frm-field').hide();
        \$('.frm-field-0').show();

        \$('#input-store').change(function () {
            \$('.frm-field').hide();
            var store = \$(this).val();
            \$('.frm-field-' + store).show();
        })


        \$('.toggle.btn').on('click', function () {
            var ckb_status = parseInt(\$(this).find('.ckb-switch').val());
            if(ckb_status == 1) {
                \$(this).find('.ckb-switch').val('0');
            } else {
                \$(this).find('.ckb-switch').val('1');
            }
        });

        ";
        // line 778
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 779
            echo "            /* Body */
            var body_font_css_link_";
            // line 780
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = \$('#body-font-family-link-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var body_font_stylesheet_link_";
            // line 781
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = '<link href=\"' + body_font_css_link_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " + '\" rel=\"stylesheet\">';
            var body_family_";
            // line 782
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " =  \$('#body-font-family-name-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var body_category_";
            // line 783
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " =  \$('#body-font-family-cate-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var body_font_weight";
            // line 784
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = \$('#body-font-weight-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();

            \$('.body-stylesheet-link').append(body_font_stylesheet_link_";
            // line 786
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");
            \$('.font-body-demo-";
            // line 787
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').css(\"font-family\", \"'\" + body_family_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " + \"', \" + body_category_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");
            \$('.font-body-demo-";
            // line 788
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').css(\"font-weight\", body_font_weight";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");

            /* Heading */
            var heading_font_css_link_";
            // line 791
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = \$('#heading-font-family-link-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var heading_font_stylesheet_link_";
            // line 792
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = '<link href=\"' + heading_font_css_link_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " + '\" rel=\"stylesheet\">';
            var heading_family_";
            // line 793
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " =  \$('#heading-font-family-name-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var heading_category_";
            // line 794
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " =  \$('#heading-font-family-cate-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();
            var heading_font_weight";
            // line 795
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " = \$('#heading-font-weight-";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').val();

            \$('.heading-stylesheet-link').append(heading_font_stylesheet_link_";
            // line 797
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");
            \$('.font-heading-demo-";
            // line 798
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').css(\"font-family\", \"'\" + heading_family_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo " + \"', \" + heading_category_";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");
            \$('.font-heading-demo-";
            // line 799
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "').css(\"font-weight\", heading_font_weight";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo ");
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 801
        echo "    });

    function chooseBodyFont(id, store_id) {
        var family = \$(\"#body-font-\" + id).data('family');
        var family_val = \$(\"#body-font-\" + id).data('family-val');
        var variants = \$(\"#body-font-\" + id).data('variants');
        var subsets = \$(\"#body-font-\" + id).data('subsets');
        var category = \$(\"#body-font-\" + id).data('category');

        var font_css_link = 'https://fonts.googleapis.com/css?family=' + family_val + \":\" + variants + '&subset=' + subsets;
        var font_stylesheet_link = '<link href=\"' + font_css_link + '\" rel=\"stylesheet\">';

        \$('.body-stylesheet-link').append(font_stylesheet_link);

        \$('#body-font-family-name-' + store_id).val(family);
        \$('#body-font-family-cate-' + store_id).val(category);
        \$('#body-font-family-link-' + store_id).val(font_css_link);

        \$('.font-body-demo-' + store_id).css(\"font-family\", \"'\" + family + \"', \" + category);
    }

    function chooseBodyWeight(value, store_id) {
        \$('.font-body-demo-' + store_id).css(\"font-weight\", value);
    }

    function chooseHeadingFont(id, store_id) {
        var family = \$(\"#heading-font-\" + id).data('family');
        var family_val = \$(\"#heading-font-\" + id).data('family-val');
        var variants = \$(\"#heading-font-\" + id).data('variants');
        var subsets = \$(\"#heading-font-\" + id).data('subsets');
        var category = \$(\"#heading-font-\" + id).data('category');

        var font_css_link = 'https://fonts.googleapis.com/css?family=' + family_val + \":\" + variants + '&subset=' + subsets;
        var font_stylesheet_link = '<link href=\"' + font_css_link + '\" rel=\"stylesheet\">';

        \$('.heading-stylesheet-link').append(font_stylesheet_link);

        \$('#heading-font-family-name-' + store_id).val(family);
        \$('#heading-font-family-cate-' + store_id).val(category);
        \$('#heading-font-family-link-' + store_id).val(font_css_link);

        \$('.font-heading-demo-' + store_id).css(\"font-family\", \"'\" + family + \"', \" + category);
    }

    function chooseHeadingWeight(value, store_id) {
        \$('.font-heading-demo-' + store_id).css(\"font-weight\", value);
    }
</script>

<script type=\"text/javascript\"><!--
    \$('#button-import').on('click', function() {

        \$('#form-data input[name=\\'file\\']').val(\$('#input-theme').val());

        \$('#form-data').submit();
    });
    //--></script>
";
        // line 858
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "plaza/module/ptcontrolpanel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2618 => 858,  2559 => 801,  2549 => 799,  2541 => 798,  2537 => 797,  2530 => 795,  2524 => 794,  2518 => 793,  2512 => 792,  2506 => 791,  2498 => 788,  2490 => 787,  2486 => 786,  2479 => 784,  2473 => 783,  2467 => 782,  2461 => 781,  2455 => 780,  2452 => 779,  2448 => 778,  2411 => 743,  2398 => 738,  2393 => 736,  2383 => 731,  2378 => 729,  2368 => 722,  2363 => 719,  2352 => 717,  2348 => 716,  2340 => 711,  2334 => 709,  2330 => 708,  2325 => 705,  2308 => 698,  2300 => 697,  2294 => 696,  2290 => 695,  2283 => 693,  2271 => 688,  2263 => 687,  2257 => 686,  2253 => 685,  2246 => 683,  2234 => 678,  2226 => 677,  2220 => 676,  2216 => 675,  2209 => 673,  2197 => 668,  2189 => 667,  2183 => 666,  2179 => 665,  2172 => 663,  2160 => 658,  2152 => 657,  2146 => 656,  2142 => 655,  2135 => 653,  2123 => 648,  2115 => 647,  2109 => 646,  2105 => 645,  2098 => 643,  2093 => 641,  2082 => 637,  2074 => 636,  2068 => 635,  2064 => 634,  2057 => 632,  2045 => 627,  2037 => 626,  2031 => 625,  2027 => 624,  2020 => 622,  2008 => 617,  2000 => 616,  1994 => 615,  1990 => 614,  1983 => 612,  1971 => 607,  1963 => 606,  1957 => 605,  1953 => 604,  1946 => 602,  1934 => 597,  1926 => 596,  1920 => 595,  1916 => 594,  1909 => 592,  1904 => 590,  1892 => 585,  1884 => 584,  1878 => 583,  1874 => 582,  1867 => 580,  1855 => 575,  1847 => 574,  1841 => 573,  1837 => 572,  1830 => 570,  1825 => 568,  1816 => 564,  1810 => 563,  1804 => 562,  1798 => 560,  1794 => 559,  1789 => 556,  1775 => 550,  1769 => 549,  1763 => 548,  1757 => 547,  1751 => 546,  1744 => 544,  1732 => 539,  1724 => 538,  1718 => 537,  1711 => 535,  1700 => 531,  1692 => 530,  1686 => 529,  1682 => 528,  1675 => 526,  1663 => 521,  1653 => 518,  1648 => 516,  1636 => 511,  1628 => 510,  1620 => 509,  1614 => 508,  1607 => 506,  1595 => 501,  1587 => 500,  1581 => 499,  1577 => 498,  1570 => 496,  1557 => 490,  1549 => 489,  1545 => 488,  1540 => 486,  1531 => 484,  1523 => 483,  1517 => 482,  1513 => 481,  1506 => 479,  1494 => 474,  1486 => 473,  1480 => 472,  1476 => 471,  1469 => 469,  1457 => 464,  1449 => 463,  1443 => 462,  1439 => 461,  1432 => 459,  1420 => 454,  1412 => 453,  1406 => 452,  1402 => 451,  1395 => 449,  1389 => 447,  1385 => 446,  1380 => 443,  1370 => 438,  1364 => 437,  1352 => 435,  1349 => 434,  1345 => 433,  1339 => 432,  1332 => 430,  1320 => 425,  1310 => 422,  1303 => 420,  1291 => 415,  1283 => 414,  1277 => 413,  1273 => 412,  1266 => 410,  1254 => 405,  1246 => 404,  1240 => 403,  1236 => 402,  1229 => 400,  1216 => 396,  1209 => 394,  1196 => 388,  1188 => 387,  1182 => 386,  1175 => 384,  1163 => 379,  1155 => 378,  1149 => 377,  1145 => 376,  1138 => 374,  1126 => 369,  1118 => 368,  1112 => 367,  1108 => 366,  1101 => 364,  1089 => 359,  1081 => 358,  1075 => 357,  1071 => 356,  1064 => 354,  1052 => 349,  1044 => 348,  1038 => 347,  1034 => 346,  1027 => 344,  1015 => 339,  1007 => 338,  1001 => 337,  997 => 336,  990 => 334,  984 => 332,  980 => 331,  973 => 327,  968 => 326,  953 => 319,  948 => 317,  938 => 312,  933 => 310,  923 => 305,  918 => 303,  908 => 298,  903 => 296,  898 => 294,  888 => 289,  883 => 287,  873 => 282,  868 => 280,  863 => 278,  853 => 273,  848 => 271,  837 => 265,  831 => 264,  825 => 263,  819 => 262,  813 => 261,  805 => 260,  800 => 258,  789 => 254,  781 => 253,  773 => 252,  768 => 249,  759 => 246,  751 => 245,  742 => 244,  735 => 243,  731 => 242,  727 => 241,  721 => 240,  716 => 238,  706 => 231,  699 => 227,  689 => 222,  684 => 220,  673 => 214,  667 => 213,  661 => 212,  655 => 211,  649 => 210,  641 => 209,  636 => 207,  626 => 202,  621 => 200,  610 => 196,  602 => 195,  594 => 194,  589 => 191,  580 => 188,  572 => 187,  563 => 186,  556 => 185,  552 => 184,  548 => 183,  542 => 182,  537 => 180,  527 => 173,  520 => 169,  511 => 165,  505 => 164,  499 => 163,  493 => 162,  487 => 160,  483 => 159,  478 => 156,  463 => 151,  457 => 150,  452 => 148,  440 => 143,  434 => 142,  430 => 141,  426 => 140,  421 => 138,  409 => 133,  403 => 132,  399 => 131,  395 => 130,  390 => 128,  378 => 123,  372 => 122,  368 => 121,  364 => 120,  359 => 118,  346 => 112,  338 => 111,  334 => 110,  329 => 108,  316 => 102,  308 => 101,  300 => 100,  292 => 99,  288 => 98,  283 => 96,  277 => 94,  273 => 93,  266 => 89,  260 => 86,  252 => 81,  248 => 80,  244 => 79,  240 => 78,  236 => 77,  232 => 76,  224 => 70,  209 => 68,  205 => 67,  199 => 64,  189 => 56,  183 => 55,  180 => 54,  168 => 52,  163 => 49,  148 => 47,  144 => 46,  136 => 45,  126 => 44,  119 => 43,  116 => 42,  111 => 41,  109 => 40,  103 => 37,  96 => 32,  88 => 28,  85 => 27,  77 => 23,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*     <div class="page-header">*/
/*         <div class="container-fluid">*/
/*             <div class="pull-right">*/
/*                 <button type="submit" form="form-ptcontrolpanel" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*                 <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*             <h1>{{ heading_title }}</h1>*/
/*             <ul class="breadcrumb theme-option-breadcrumb">*/
/*                 {% for breadcrumb in breadcrumbs %}*/
/*                     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container-fluid">*/
/*         {% if error_warning %}*/
/*             <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endif %}*/
/*         {% if error_load_file %}*/
/*             <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_load_file }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endif %}*/
/*         {% if success %}*/
/*             <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endif %}*/
/*         <div class="row">*/
/*             <div class="col-md-3 col-sm-12">*/
/*                 <div class="theme-option-container">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title">{{ text_dashboard_menu }}</h3>*/
/*                         </div>*/
/*                         <ul class="nav nav-tabs menu-sections pt-dashboard-menu">*/
/*                             {% set i = 0 %}*/
/*                             {% for menu in plaza_menus %}*/
/*                                 {% if menu.child %}*/
/*                                     <li {% if menu.active %} class="active" {% endif %}>*/
/*                                         <a href="#ptcollapse_{{ i }}" data-toggle="collapse" class="parent {% if not menu.active %} collapsed {% endif %}">{{ menu.title }}</a>*/
/*                                         <ul id="ptcollapse_{{ i }}" class="collapse {% if menu.active %} in {% endif %}">*/
/*                                             {% for item in menu.child %}*/
/*                                                 <li {% if item.active %} class="active" {% endif %}><a href="{{ item.url }}">{{ item.title }}</a></li>*/
/*                                             {% endfor %}*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                 {% else %}*/
/*                                     <li {% if menu.active %} class="active" {% endif %}><a href="{{ menu.url }}">{{ menu.title }}</a></li>*/
/*                                 {% endif %}*/
/*                                 {% set i = i + 1 %}*/
/*                             {% endfor %}*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-md-9 col-sm-12">*/
/*                 <div class="theme-option-container">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title"><i class="fa fa-magic"></i> {{ text_edit }}</h3>*/
/*                             <div class="panel-select">*/
/*                                 <select id="input-store" class="form-control">*/
/*                                     {% for store in stores %}*/
/*                                         <option value="{{ store.store_id }}" {% if(store.store_id == 0) %}selected="selected"{% endif %}>{{ store.name }}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-3 col-left">*/
/*                                 <ul class="nav nav-tabs menu-sections">*/
/*                                     <li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i>{{ tab_general }}</a></li>*/
/*                                     <li><a href="#tab-font" data-toggle="tab"><i class="fa fa-font" aria-hidden="true"></i>{{ tab_font }}</a></li>*/
/*                                     <li><a href="#tab-catalog" data-toggle="tab"><i class="fa fa-shopping-bag" aria-hidden="true"></i>{{ tab_catalog }}</a></li>*/
/*                                     <li><a href="#tab-product" data-toggle="tab"><i class="fa fa-cube" aria-hidden="true"></i>{{ tab_product }}</a></li>*/
/*                                     <li><a href="#tab-category" data-toggle="tab"><i class="fa fa-tags" aria-hidden="true"></i>{{ tab_category }}</a></li>*/
/*                                     <li><a href="#tab-advance" data-toggle="tab"><i class="fa fa-fire" aria-hidden="true"></i>{{ tab_advance }}</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-md-9 col-right">*/
/*                                 <div class="panel-body">*/
/*                                     <form action="{{ action_import }}" method="post" enctype="multipart/form-data" id="form-data" class="form-horizontal">*/
/*                                         <input type="hidden" name="file" />*/
/*                                     </form>*/
/*                                     <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-ptcontrolpanel" class="form-horizontal">*/
/*                                         <input type="hidden" name="module_ptcontrolpanel_status" value="1" />*/
/*                                         <div class="tab-content">*/
/*                                             <div class="tab-pane active" id="tab-general">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_header }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <select name="module_ptcontrolpanel_header_layout[{{ store.store_id }}]" class="form-control">*/
/*                                                                     <option value="1" {% if module_ptcontrolpanel_header_layout[store.store_id] == '1' %} selected="selected" {% endif %}>{{ entry_header }} 1</option>*/
/*                                                                     <option value="2" {% if module_ptcontrolpanel_header_layout[store.store_id] == '2' %} selected="selected" {% endif %}>{{ entry_header }} 2</option>*/
/*                                                                     <option value="3" {% if module_ptcontrolpanel_header_layout[store.store_id] == '3' %} selected="selected" {% endif %}>{{ entry_header }} 3</option>*/
/*                                                                     <option value="4" {% if module_ptcontrolpanel_header_layout[store.store_id] == '4' %} selected="selected" {% endif %}>{{ entry_header }} 4</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_responsive }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <select name="module_ptcontrolpanel_responsive_type[{{ store.store_id }}]" class="form-control">*/
/*                                                                     <option value="responsive" {% if module_ptcontrolpanel_responsive_type[store.store_id] == 'responsive' %} selected="selected" {% endif %}>{{ text_responsive_layout }}</option>*/
/*                                                                     <option value="specified" {% if module_ptcontrolpanel_responsive_type[store.store_id] == 'specified' %} selected="selected" {% endif %}>{{ text_specified_layout }}</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_sticky_header }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_sticky_header[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_sticky_header[{{ store.store_id }}]"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36"*/
/*                                                                         {% if module_ptcontrolpanel_sticky_header[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_scroll_top }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_scroll_top[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_scroll_top[{{ store.store_id }}]"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36"*/
/*                                                                         {% if module_ptcontrolpanel_scroll_top[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_lazy_load }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_lazy_load[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_lazy_load[{{ store.store_id }}]"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36"*/
/*                                                                         {% if module_ptcontrolpanel_lazy_load[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_loader_image }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <a href="" id="thumb-image{{ store.store_id }}" data-toggle="image" class="img-thumbnail"><img src="{{ thumb[store.store_id] }}" alt="" title=""  /></a>*/
/*                                                                 <input type="hidden" id="input-image{{ store.store_id }}" name="module_ptcontrolpanel_loader_img[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_loader_img[store.store_id] }}" />*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/* */
/*                                             <div class="tab-pane" id="tab-font">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <ul class="nav nav-tabs hoz-ul-sections">*/
/*                                                             <li class="active"><a href="#tab-body-{{ store.store_id }}" data-toggle="tab">{{ tab_body }}</a></li>*/
/*                                                             <li><a href="#tab-header-{{ store.store_id }}" data-toggle="tab">{{ tab_heading }}</a></li>*/
/*                                                             <li><a href="#tab-link-{{ store.store_id }}" data-toggle="tab">{{ tab_link }}</a></li>*/
/*                                                             <li><a href="#tab-button-{{ store.store_id }}" data-toggle="tab">{{ tab_button }}</a></li>*/
/*                                                         </ul>*/
/* */
/*                                                         <div class="tab-content child-content">*/
/*                                                             <div class="tab-pane active" id="tab-body-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-sm-3"></div>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <p class="font-body-demo-{{ store.store_id }} font-demo">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br />*/
/*                                                                             a b c d e f g h i j k l m n o p q r s t u v w x y z <br />*/
/*                                                                             0 1 2 3 4 5 6 7 8 9</p>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group font-control">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_global_font }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <select class="form-control" name="module_ptcontrolpanel_body_font_family_id[{{ store.store_id }}]" onchange="chooseBodyFont(this.value, {{ store.store_id }})">*/
/*                                                                             <option>{{ text_choose_font }}</option>*/
/*                                                                             {% for font in fonts %}*/
/*                                                                                 <option value="{{ font.id }}" {% if module_ptcontrolpanel_body_font_family_id[store.store_id] == font.id %} selected="selected" {% endif %}*/
/*                                                                                         id="body-font-{{ font.id }}" data-family="{{ font.family }}" data-family-val="{{ font.family_val }}"*/
/*                                                                                         data-variants="{{ font.variants }}" data-subsets="{{ font.subsets }}" data-category="{{ font.category }}">*/
/*                                                                                     {{ font.family }}*/
/*                                                                                 </option>*/
/*                                                                             {% endfor %}*/
/*                                                                         </select>*/
/*                                                                     </div>*/
/* */
/*                                                                     <input type="hidden" id="body-font-family-name-{{ store.store_id }}" name="module_ptcontrolpanel_body_font_family_name[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_body_font_family_name[store.store_id] }}" />*/
/*                                                                     <input type="hidden" id="body-font-family-cate-{{ store.store_id }}" name="module_ptcontrolpanel_body_font_family_cate[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_body_font_family_cate[store.store_id] }}" />*/
/*                                                                     <input type="hidden" id="body-font-family-link-{{ store.store_id }}" name="module_ptcontrolpanel_body_font_family_link[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_body_font_family_link[store.store_id] }}" />*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_font_size }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input class="form-control" type="text" name="module_ptcontrolpanel_body_font_size[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_body_font_size[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_font_weight }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <select class="form-control" id="body-font-weight-{{ store.store_id }}" name="module_ptcontrolpanel_body_font_weight[{{ store.store_id }}]" onclick="chooseBodyWeight(this.value, {{ store.store_id }})">*/
/*                                                                             <option value="300" {% if module_ptcontrolpanel_body_font_weight[store.store_id] == '300' %}selected="selected"{% endif %}>300</option>*/
/*                                                                             <option value="400" {% if module_ptcontrolpanel_body_font_weight[store.store_id] == '400' %}selected="selected"{% endif %}>400</option>*/
/*                                                                             <option value="500" {% if module_ptcontrolpanel_body_font_weight[store.store_id] == '500' %}selected="selected"{% endif %}>500</option>*/
/*                                                                             <option value="600" {% if module_ptcontrolpanel_body_font_weight[store.store_id] == '600' %}selected="selected"{% endif %}>600</option>*/
/*                                                                             <option value="700" {% if module_ptcontrolpanel_body_font_weight[store.store_id] == '700' %}selected="selected"{% endif %}>700</option>*/
/*                                                                         </select>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_body_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_body_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/* */
/*                                                             <div class="tab-pane" id="tab-header-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-sm-3"></div>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <p class="font-heading-demo-{{ store.store_id }} font-demo">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br />*/
/*                                                                             a b c d e f g h i j k l m n o p q r s t u v w x y z <br />*/
/*                                                                             0 1 2 3 4 5 6 7 8 9</p>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group font-control">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_heading_font }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <select class="form-control" name="module_ptcontrolpanel_heading_font_family_id[{{ store.store_id }}]" onchange="chooseHeadingFont(this.value, {{ store.store_id }})">*/
/*                                                                             <option>{{ text_choose_font }}</option>*/
/*                                                                             {% for font in fonts %}*/
/*                                                                                 <option value="{{ font.id }}" {% if module_ptcontrolpanel_heading_font_family_id[store.store_id] == font.id %} selected="selected" {% endif %}*/
/*                                                                                         id="heading-font-{{ font.id }}" data-family="{{ font.family }}" data-family-val="{{ font.family_val }}"*/
/*                                                                                         data-variants="{{ font.variants }}" data-subsets="{{ font.subsets }}" data-category="{{ font.category }}">*/
/*                                                                                     {{ font.family }}*/
/*                                                                                 </option>*/
/*                                                                             {% endfor %}*/
/*                                                                         </select>*/
/*                                                                     </div>*/
/* */
/*                                                                     <input type="hidden" id="heading-font-family-name-{{ store.store_id }}" name="module_ptcontrolpanel_heading_font_family_name[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_heading_font_family_name[store.store_id] }}" />*/
/*                                                                     <input type="hidden" id="heading-font-family-cate-{{ store.store_id }}" name="module_ptcontrolpanel_heading_font_family_cate[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_heading_font_family_cate[store.store_id] }}" />*/
/*                                                                     <input type="hidden" id="heading-font-family-link-{{ store.store_id }}" name="module_ptcontrolpanel_heading_font_family_link[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_heading_font_family_link[store.store_id] }}" />*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_font_weight }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <select class="form-control" id="heading-font-weight-{{ store.store_id }}" name="module_ptcontrolpanel_heading_font_weight[{{ store.store_id }}]" onchange="chooseHeadingWeight(this.value, {{ store.store_id }})">*/
/*                                                                             <option value="300" {% if module_ptcontrolpanel_heading_font_weight[store.store_id] == '300' %}selected="selected"{% endif %}>300</option>*/
/*                                                                             <option value="400" {% if module_ptcontrolpanel_heading_font_weight[store.store_id] == '400' %}selected="selected"{% endif %}>400</option>*/
/*                                                                             <option value="500" {% if module_ptcontrolpanel_heading_font_weight[store.store_id] == '500' %}selected="selected"{% endif %}>500</option>*/
/*                                                                             <option value="600" {% if module_ptcontrolpanel_heading_font_weight[store.store_id] == '600' %}selected="selected"{% endif %}>600</option>*/
/*                                                                             <option value="700" {% if module_ptcontrolpanel_heading_font_weight[store.store_id] == '700' %}selected="selected"{% endif %}>700</option>*/
/*                                                                         </select>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-3 control-label">{{ entry_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_heading_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_heading_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/* */
/*                                                             <div class="tab-pane" id="tab-link-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_link_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_link_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_hover_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_link_hover_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_link_hover_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/* */
/*                                                             <div class="tab-pane" id="tab-button-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_button_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_button_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_hover_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_button_hover_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_button_hover_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_bg_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_button_bg_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_button_bg_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label">{{ entry_bg_hover_color }}</label>*/
/*                                                                     <div class="col-sm-3">*/
/*                                                                         <input class="form-control jscolor" type="text" name="module_ptcontrolpanel_button_bg_hover_color[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_button_bg_hover_color[store.store_id] }}" />*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                                 <input type="hidden" value="{{ entry_font_weight }}" id="text-font-weight" />*/
/*                                                 <input type="hidden" value="{{ entry_font_subset }}" id="text-font-subset" />*/
/*                                             </div>*/
/* */
/*                                             <div class="tab-pane" id="tab-product">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-related-{{ store.store_id }}">{{ entry_related_pro }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_related[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_related[{{ store.store_id }}]" id="input-related-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_related[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-social-{{ store.store_id }}">{{ entry_social_icons }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_social[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_social[{{ store.store_id }}]" id="input-social-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_social[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-tax-{{ store.store_id }}">{{ entry_tax }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_tax[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_tax[{{ store.store_id }}]" id="input-tax-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_tax[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-tags-{{ store.store_id }}">{{ entry_tags }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_tags[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_tags[{{ store.store_id }}]" id="input-tags-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_tags[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-zoom-{{ store.store_id }}">{{ entry_use_zoom }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_use_zoom[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch zoom-status" name="module_ptcontrolpanel_use_zoom[{{ store.store_id }}]" id="input-zoom-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_use_zoom[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-zoom-type-{{ store.store_id }}">{{ entry_zoom_type }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <select name="module_ptcontrolpanel_zoom_type[{{ store.store_id }}]" id="input-zoom-type-{{ store.store_id }}" class="form-control">*/
/*                                                                     <option value="outer" {% if module_ptcontrolpanel_zoom_type[store.store_id] == 'outer' %}selected="selected"{% endif %}>{{ text_outside }}</option>*/
/*                                                                     <option value="inner" {% if module_ptcontrolpanel_zoom_type[store.store_id] == 'inner' %}selected="selected"{% endif %}>{{ text_inside }}</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-zoom-space-{{ store.store_id }}">{{ entry_zoom_space }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="text" id="input-zoom-space-{{ store.store_id }}" class="form-control" name="module_ptcontrolpanel_zoom_space[{{ store.store_id }}]" value="{{ module_ptcontrolpanel_zoom_space[store.store_id] }}" placeholder="{{ text_zoom_space }}" />*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-zoom-title-{{ store.store_id }}">{{ entry_zoom_title }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_zoom_title[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_zoom_title[{{ store.store_id }}]" id="input-zoom-title-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_zoom_title[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-swatches-{{ store.store_id }}">{{ entry_use_swatches }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_use_swatches[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_use_swatches[{{ store.store_id }}]" id="input-swatches-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_use_swatches[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-dimension-{{ store.store_id }}">{{ entry_icon_swatches }}</label>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <input type="text" value="{{ module_ptcontrolpanel_swatches_width[store.store_id] }}" name="module_ptcontrolpanel_swatches_width[{{ store.store_id }}]" placeholder="{{ entry_width }}" class="form-control" />*/
/*                                                             </div>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <input type="text" value="{{ module_ptcontrolpanel_swatches_height[store.store_id] }}" name="module_ptcontrolpanel_swatches_height[{{ store.store_id }}]" placeholder="{{ entry_height }}" class="form-control" />*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-color-option-{{ store.store_id }}">{{ entry_color_option }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <select name="module_ptcontrolpanel_swatches_option[{{ store.store_id }}]" id="input-color-option-{{ store.store_id }}" class="form-control">*/
/*                                                                     {% for option in options %}*/
/*                                                                         {% if option.type == 'select' or option.type == 'radio' %}*/
/*                                                                             <option value="{{ option.option_id }}" {% if module_ptcontrolpanel_swatches_option[store.store_id] == option.option_id %} selected="selected" {% endif %}>{{ option.name }}</option>*/
/*                                                                         {% endif %}*/
/*                                                                     {% endfor %}*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/* */
/*                                             <div class="tab-pane" id="tab-category">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-category-image-{{ store.store_id }}">{{ entry_category_image }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_category_image[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_image[{{ store.store_id }}]" id="input-category-image-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_category_image[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-category-des-{{ store.store_id }}">{{ entry_category_des }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_category_description[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_description[{{ store.store_id }}]" id="input-category-des-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_category_description[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-category-sub-{{ store.store_id }}">{{ entry_sub_category }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_sub_category[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_sub_category[{{ store.store_id }}]" id="input-category-sub-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_sub_category[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-filter-{{ store.store_id }}">{{ entry_filter }}</label>*/
/*                                                             <div class="col-sm-2">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_use_filter[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_use_filter[{{ store.store_id }}]" id="input-filter-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_use_filter[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                             <label class="col-sm-2 control-label">{{ entry_position }}</label>*/
/*                                                             <div class="col-sm-2">*/
/*                                                                 <select name="module_ptcontrolpanel_filter_position[{{ store.store_id }}]" class="form-control">*/
/*                                                                     <option value="left" {% if module_ptcontrolpanel_filter_position[store.store_id] == 'left' %} selected="selected" {% endif %}>{{ text_left }}</option>*/
/*                                                                     <option value="right" {% if module_ptcontrolpanel_filter_position[store.store_id] == 'right' %} selected="selected" {% endif %}>{{ text_right }}</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-cquickview-{{ store.store_id }}">{{ entry_quickview }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_cate_quickview[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_cate_quickview[{{ store.store_id }}]" id="input-cquickview-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_cate_quickview[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-image-effect-{{ store.store_id }}">{{ entry_img_effect }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <select name="module_ptcontrolpanel_img_effect[{{ store.store_id }}]" id="input-image-effect-{{ store.store_id }}" class="form-control">*/
/*                                                                     <option value="none" {% if module_ptcontrolpanel_img_effect[store.store_id] == 'none' %}selected="selected" {% endif %}>{{ text_none }}</option>*/
/*                                                                     <option value="hover" {% if module_ptcontrolpanel_img_effect[store.store_id] == 'hover' %}selected="selected" {% endif %}>{{ text_hover_img }}</option>*/
/*                                                                     <option value="swatches" {% if module_ptcontrolpanel_img_effect[store.store_id] == 'swatches' %}selected="selected" {% endif %}>{{ text_swatches_img }}</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label">{{ entry_icon_swatches }}</label>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <input type="text" value="{{ module_ptcontrolpanel_cate_swatches_width[store.store_id] }}" name="module_ptcontrolpanel_cate_swatches_width[{{ store.store_id }}]" placeholder="{{ entry_width }}" class="form-control" />*/
/*                                                             </div>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <input type="text" value="{{ module_ptcontrolpanel_cate_swatches_height[store.store_id] }}" name="module_ptcontrolpanel_cate_swatches_height[{{ store.store_id }}]" placeholder="{{ entry_height }}" class="form-control" />*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-view-{{ store.store_id }}">{{ entry_advance_view }}</label>*/
/*                                                             <div class="col-sm-6">*/
/*                                                                 <input type="hidden" name="module_ptcontrolpanel_advance_view[{{ store.store_id }}]" value="0" />*/
/*                                                                 <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_advance_view[{{ store.store_id }}]" id="input-view-{{ store.store_id }}"*/
/*                                                                        data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                         {% if module_ptcontrolpanel_advance_view[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-default-view-{{ store.store_id }}">{{ entry_default_view }}</label>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <select name="module_ptcontrolpanel_default_view[{{ store.store_id }}]" id="input-default-view-{{ store.store_id }}" class="form-control">*/
/*                                                                     <option value="list" {% if module_ptcontrolpanel_default_view[store.store_id] == 'list' %} selected="selected" {% endif %}>{{ text_list }}</option>*/
/*                                                                     <option value="grid" {% if module_ptcontrolpanel_default_view[store.store_id] == 'grid' %} selected="selected" {% endif %}>{{ text_grid }}</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-4 control-label" for="input-row-{{ store.store_id }}">{{ entry_product_row }}</label>*/
/*                                                             <div class="col-sm-3">*/
/*                                                                 <select name="module_ptcontrolpanel_product_row[{{ store.store_id }}]" id="input-row-{{ store.store_id }}" class="form-control">*/
/*                                                                     <option value="2" {% if module_ptcontrolpanel_product_row[store.store_id] == '2' %} selected="selected" {% endif %}>2</option>*/
/*                                                                     <option value="3" {% if module_ptcontrolpanel_product_row[store.store_id] == '3' %} selected="selected" {% endif %}>3</option>*/
/*                                                                     <option value="4" {% if module_ptcontrolpanel_product_row[store.store_id] == '4' %} selected="selected" {% endif %}>4</option>*/
/*                                                                     <option value="5" {% if module_ptcontrolpanel_product_row[store.store_id] == '5' %} selected="selected" {% endif %}>5</option>*/
/*                                                                 </select>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/* */
/*                                             <div class="tab-pane" id="tab-catalog">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <ul class="nav nav-tabs hoz-ul-sections">*/
/*                                                             <li class="active"><a href="#tab-cheader-{{ store.store_id }}" data-toggle="tab">{{ tab_header }}</a></li>*/
/*                                                             <li><a href="#tab-product-{{ store.store_id }}" data-toggle="tab">{{ tab_product }}</a></li>*/
/*                                                             <li><a href="#tab-category-{{ store.store_id }}" data-toggle="tab">{{ tab_product_listing }}</a></li>*/
/*                                                         </ul>*/
/* */
/*                                                         <div class="tab-content child-content">*/
/*                                                             <div class="tab-pane active" id="tab-cheader-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-header-cart-{{ store.store_id }}">{{ entry_cart }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_header_cart[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_header_cart[{{ store.store_id }}]" id="input-header-cart-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_header_cart[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-header-currency-{{ store.store_id }}">{{ entry_currency }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_header_currency[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_header_currency[{{ store.store_id }}]" id="input-header-currency-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_header_currency[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/* */
/*                                                             <div class="tab-pane" id="tab-product-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-product-price-{{ store.store_id }}">{{ entry_show_price }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_product_price[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_product_price[{{ store.store_id }}]" id="input-product-price-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_product_price[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-product-cart-{{ store.store_id }}">{{ entry_show_cart }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_product_cart[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_product_cart[{{ store.store_id }}]" id="input-product-cart-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_product_cart[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-product-wishlist-{{ store.store_id }}">{{ entry_show_wishlist }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_product_wishlist[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_product_wishlist[{{ store.store_id }}]" id="input-product-wishlist-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_product_wishlist[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-product-compare-{{ store.store_id }}">{{ entry_show_compare }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_product_compare[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_product_compare[{{ store.store_id }}]" id="input-product-compare-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_product_compare[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-product-options-{{ store.store_id }}">{{ entry_show_options }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_product_options[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_product_options[{{ store.store_id }}]" id="input-product-options-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_product_options[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="tab-pane" id="tab-category-{{ store.store_id }}">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-price-{{ store.store_id }}">{{ entry_show_price }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_price[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_price[{{ store.store_id }}]" id="input-category-price-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_price[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-cart-{{ store.store_id }}">{{ entry_show_cart }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_cart[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_cart[{{ store.store_id }}]" id="input-category-cart-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_cart[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-wishlist-{{ store.store_id }}">{{ entry_show_wishlist }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_wishlist[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_wishlist[{{ store.store_id }}]" id="input-category-wishlist-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_wishlist[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-compare-{{ store.store_id }}">{{ entry_show_compare }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_compare[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_compare[{{ store.store_id }}]" id="input-category-compare-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_compare[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-prodes-{{ store.store_id }}">{{ entry_product_des }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_prodes[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_prodes[{{ store.store_id }}]" id="input-category-prodes-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_prodes[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="form-group">*/
/*                                                                     <label class="col-sm-4 control-label" for="input-category-label-{{ store.store_id }}">{{ entry_label }}</label>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <input type="hidden" name="module_ptcontrolpanel_category_label[{{ store.store_id }}]" value="0" />*/
/*                                                                         <input type="checkbox" class="ckb-switch" name="module_ptcontrolpanel_category_label[{{ store.store_id }}]" id="input-category-label-{{ store.store_id }}"*/
/*                                                                                data-toggle="toggle" data-on="{{ text_enabled }}" data-off="{{ text_disabled }}" data-width="100" data-height="36" data-store-id="{{ store.store_id }}"*/
/*                                                                                 {% if module_ptcontrolpanel_category_label[store.store_id] %} value="1" checked {% else %} value="0" {% endif %}>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/* */
/*                                             <div class="tab-pane" id="tab-advance">*/
/*                                                 {% for store in stores %}*/
/*                                                     <div class="frm-field frm-field-{{ store.store_id }}">*/
/*                                                         <div class="form-group" >*/
/*                                                             <label class="col-sm-3 control-label" for="input-theme">{{ entry_theme_database }}</label>*/
/*                                                             <div class="col-sm-9">*/
/*                                                                 <div class="row">*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <select id="input-theme" class="form-control" name="file">*/
/*                                                                             {% for key, value in database %}*/
/*                                                                                 <option value="{{ key }}">{{ value }}</option>*/
/*                                                                             {% endfor %}*/
/*                                                                         </select>*/
/*                                                                     </div>*/
/*                                                                     <div class="col-sm-6">*/
/*                                                                         <button type="button" id="button-import" class="btn btn-primary btn-theme-option"><i class="fa fa-upload"></i> {{ button_import }}</button>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-3 control-label" for="input-theme">{{ entry_custom_css }}</label>*/
/*                                                             <div class="col-sm-9">*/
/*                                                                 <textarea class="form-control code-area" rows="15" name="module_ptcontrolpanel_custom_css[{{ store.store_id }}]">{{ module_ptcontrolpanel_custom_css[store.store_id] }}</textarea>*/
/*                                                             </div>*/
/*                                                         </div>*/
/* */
/*                                                         <div class="form-group">*/
/*                                                             <label class="col-sm-3 control-label" for="input-theme">{{ entry_custom_js }}</label>*/
/*                                                             <div class="col-sm-9">*/
/*                                                                 <textarea class="form-control code-area" rows="15" name="module_ptcontrolpanel_custom_js[{{ store.store_id }}]">{{ module_ptcontrolpanel_custom_js[store.store_id] }}</textarea>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </form>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="body-stylesheet-link"></div>*/
/* <div class="heading-stylesheet-link"></div>*/
/* <script type="text/javascript">*/
/*     $(document).ready(function () {*/
/*         $('.frm-field').hide();*/
/*         $('.frm-field-0').show();*/
/* */
/*         $('#input-store').change(function () {*/
/*             $('.frm-field').hide();*/
/*             var store = $(this).val();*/
/*             $('.frm-field-' + store).show();*/
/*         })*/
/* */
/* */
/*         $('.toggle.btn').on('click', function () {*/
/*             var ckb_status = parseInt($(this).find('.ckb-switch').val());*/
/*             if(ckb_status == 1) {*/
/*                 $(this).find('.ckb-switch').val('0');*/
/*             } else {*/
/*                 $(this).find('.ckb-switch').val('1');*/
/*             }*/
/*         });*/
/* */
/*         {% for store in stores %}*/
/*             /* Body *//* */
/*             var body_font_css_link_{{ store.store_id }} = $('#body-font-family-link-{{ store.store_id }}').val();*/
/*             var body_font_stylesheet_link_{{ store.store_id }} = '<link href="' + body_font_css_link_{{ store.store_id }} + '" rel="stylesheet">';*/
/*             var body_family_{{ store.store_id }} =  $('#body-font-family-name-{{ store.store_id }}').val();*/
/*             var body_category_{{ store.store_id }} =  $('#body-font-family-cate-{{ store.store_id }}').val();*/
/*             var body_font_weight{{ store.store_id }} = $('#body-font-weight-{{ store.store_id }}').val();*/
/* */
/*             $('.body-stylesheet-link').append(body_font_stylesheet_link_{{ store.store_id }});*/
/*             $('.font-body-demo-{{ store.store_id }}').css("font-family", "'" + body_family_{{ store.store_id }} + "', " + body_category_{{ store.store_id }});*/
/*             $('.font-body-demo-{{ store.store_id }}').css("font-weight", body_font_weight{{ store.store_id }});*/
/* */
/*             /* Heading *//* */
/*             var heading_font_css_link_{{ store.store_id }} = $('#heading-font-family-link-{{ store.store_id }}').val();*/
/*             var heading_font_stylesheet_link_{{ store.store_id }} = '<link href="' + heading_font_css_link_{{ store.store_id }} + '" rel="stylesheet">';*/
/*             var heading_family_{{ store.store_id }} =  $('#heading-font-family-name-{{ store.store_id }}').val();*/
/*             var heading_category_{{ store.store_id }} =  $('#heading-font-family-cate-{{ store.store_id }}').val();*/
/*             var heading_font_weight{{ store.store_id }} = $('#heading-font-weight-{{ store.store_id }}').val();*/
/* */
/*             $('.heading-stylesheet-link').append(heading_font_stylesheet_link_{{ store.store_id }});*/
/*             $('.font-heading-demo-{{ store.store_id }}').css("font-family", "'" + heading_family_{{ store.store_id }} + "', " + heading_category_{{ store.store_id }});*/
/*             $('.font-heading-demo-{{ store.store_id }}').css("font-weight", heading_font_weight{{ store.store_id }});*/
/*         {% endfor %}*/
/*     });*/
/* */
/*     function chooseBodyFont(id, store_id) {*/
/*         var family = $("#body-font-" + id).data('family');*/
/*         var family_val = $("#body-font-" + id).data('family-val');*/
/*         var variants = $("#body-font-" + id).data('variants');*/
/*         var subsets = $("#body-font-" + id).data('subsets');*/
/*         var category = $("#body-font-" + id).data('category');*/
/* */
/*         var font_css_link = 'https://fonts.googleapis.com/css?family=' + family_val + ":" + variants + '&subset=' + subsets;*/
/*         var font_stylesheet_link = '<link href="' + font_css_link + '" rel="stylesheet">';*/
/* */
/*         $('.body-stylesheet-link').append(font_stylesheet_link);*/
/* */
/*         $('#body-font-family-name-' + store_id).val(family);*/
/*         $('#body-font-family-cate-' + store_id).val(category);*/
/*         $('#body-font-family-link-' + store_id).val(font_css_link);*/
/* */
/*         $('.font-body-demo-' + store_id).css("font-family", "'" + family + "', " + category);*/
/*     }*/
/* */
/*     function chooseBodyWeight(value, store_id) {*/
/*         $('.font-body-demo-' + store_id).css("font-weight", value);*/
/*     }*/
/* */
/*     function chooseHeadingFont(id, store_id) {*/
/*         var family = $("#heading-font-" + id).data('family');*/
/*         var family_val = $("#heading-font-" + id).data('family-val');*/
/*         var variants = $("#heading-font-" + id).data('variants');*/
/*         var subsets = $("#heading-font-" + id).data('subsets');*/
/*         var category = $("#heading-font-" + id).data('category');*/
/* */
/*         var font_css_link = 'https://fonts.googleapis.com/css?family=' + family_val + ":" + variants + '&subset=' + subsets;*/
/*         var font_stylesheet_link = '<link href="' + font_css_link + '" rel="stylesheet">';*/
/* */
/*         $('.heading-stylesheet-link').append(font_stylesheet_link);*/
/* */
/*         $('#heading-font-family-name-' + store_id).val(family);*/
/*         $('#heading-font-family-cate-' + store_id).val(category);*/
/*         $('#heading-font-family-link-' + store_id).val(font_css_link);*/
/* */
/*         $('.font-heading-demo-' + store_id).css("font-family", "'" + family + "', " + category);*/
/*     }*/
/* */
/*     function chooseHeadingWeight(value, store_id) {*/
/*         $('.font-heading-demo-' + store_id).css("font-weight", value);*/
/*     }*/
/* </script>*/
/* */
/* <script type="text/javascript"><!--*/
/*     $('#button-import').on('click', function() {*/
/* */
/*         $('#form-data input[name=\'file\']').val($('#input-theme').val());*/
/* */
/*         $('#form-data').submit();*/
/*     });*/
/*     //--></script>*/
/* {{ footer }}*/
